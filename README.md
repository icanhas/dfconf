# dfconf #

dfconf (WIP) is a Dwarf Fortress launcher/config program, and drop-in replacement for the one supplied with Lazy Newb Pack.

This version was written against PeridexisErrant LNP r55.

PyLNP now exists and is complete, rendering dfconf more or less redundant.

## Installation ##
### Windows ###
* Place dfconf.exe or dfconf-amd64.exe in the root Lazy Newb 
Pack directory (that is, in the same directory as 
'Lazy Newb Pack GUI.exe').

## Usage ##
dfconf can take the path to the LNP directory as an optional command-line argument. e.g.:
```
#!bash

dfconf /home/urist/df
```
